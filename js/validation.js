function validateForm() {
	var roomNumber 		= document.getElementById("id");
	var checkInDate 	= document.getElementById("dateFrom");
	var checkOutDate 	= document.getElementById("dateTo");
	var NumberOfBeds	= document.getElementById("beds");
	var Price  			= document.getElementById("price");
	var firstName		= document.getElementById("firstName");
	var lastName 		= document.getElementById("lastName");
	var personalNumber 	= document.getElementById("egn");
	var personalId 		= document.getElementById("passport");
	var today 			= new Date();
	var eMail 			= document.getElementById("eMail");
	var letters         = /^[A-Za-z]+$/;
	var filter          = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	
	if( roomNumber != null) {
		roomNumber = roomNumber.value;
		
		/*if (roomNumber == null || roomNumber == "") {
	        alert("Room number must be filled out");
	        return false;
	    }else*/ if (roomNumber >= 1000){
			alert("Please enter correct Room number");
			return false;
		};
	}
	
	if( NumberOfBeds != null) {
		NumberOfBeds = NumberOfBeds.value;
		if (NumberOfBeds == null || NumberOfBeds == "") {
	        alert("Number of beds must be filled out");
	        return false;
	    }else if (NumberOfBeds > 3){
			alert("Please enter correct number of beds!");
			return false;
		};
	}

	if( Price != null) {
		Price = Price.value;
		
		if (Price == null || Price == "") {
	        alert("Price must be filled out");
	        return false;
	    }else if (Price.length <= 0 ){
			alert("Please enter correct price!");
			return false;
		};
	}
	
	
	if( (checkInDate != null)& (checkOutDate != null)) {
		checkOutDate = checkOutDate.value;
		checkInDate = checkInDate.value;
		
		if( (checkInDate < today) || (checkOutDate < today) ){
			alert("The dates must be bigger than today");
			return false;
		} else if(checkInDate > checkOutDate){
			alert("The start day must be bigger than the check out day");
			return false;
		};
	}
	
	
	
	if( firstName != null) {
		firstName = firstName.value;
		
	    if (firstName == null || firstName == "") {
	        alert("Name must be filled out");
	        return false;
	    }
	    else if(firstName.length > 20){
			alert("Please enter only your first name");
			return false;
		}else if(firstName.match(letters))
	    {
	        return true;
	    }else {	    
	        alert('First name must have alphabet characters only');
	        return false;
	    };
	}
	
	
	if(eMail!=null){
		if (!filter.test(eMail.value)) {
	        alert('Please provide a valid email address');
	        eMail.focus;
	        return false;
		}
	};

	if( lastName != null) {
		lastName = lastName.value;
		
	    if ( lastName == null || lastName == "") {
	        alert("Name must be filled out");
	        return false;
	    }else if(lastName.length > 20){
			alert("Please enter only your last name");
			return false;
		};
	}


	if( personalNumber != null) {
		personalNumber = personalNumber.value;
		
		if(personalNumber.length > 20){
			alert("Please enter correct PIN");
			return false;
		};	
	}
	 
	if( personalId != null) {
		personalId = personalId.value;
		
		if (personalId.length != 9){
			alert("Please enter correct ID number");
			return false;
		};
	}
	
	return true;
}
