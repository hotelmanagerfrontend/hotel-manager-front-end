/// <reference path="typings/jquery/jquery.d.ts"/>

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
var url="http://jsonstub.com/"

/*$("#addingRoom").submit(function () {
	var formData = JSON.stringify($("#addingRoom").serializeObject());
	console.log(formData);
	if (validateForm()) {
		$.ajax({
			type: 'POST',
			url: 'http://151.237.128.65:8080/HotelManagerTom/RoomGenerationServlet',
			data: formData,
			contentType: 'application/json'
		}).done(function (data) {
			$("#addingRoom").trigger('reset');
			alert("You created a new room!");
		}).fail(function(data){
				alert("Room exists!");
			});
	}
	return false;
});

*/

$("#addingRoom").submit(function () {
	var formData = JSON.stringify($("#addingRoom").serializeObject());
	console.log(formData);
	if (validateForm()) {
		$.ajax({
			type: 'POST',
			url: url +'addRoom',
			data: formData,
			contentType: 'application/json',
			beforeSend: function (request) {
			        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
			        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
			    }
		}).done(function (data) {
			$("#addingRoom").trigger('reset');
			alert("You created a new room!");
		
		}).fail(function(data){
				alert("Room exists!");
			});
	}
	return false;
});


/*$("#bookingForm").submit(function () {
	var formsData = JSON.stringify($("#bookingForm").serializeObject());
	console.log(formsData);
	if (validateForm()) {
		$.ajax({
			type: "POST",
			url:'http://151.237.128.65:8080/HotelManagerTom/ReservationServlet',
			data: formsData,
			contentType: "application/json"
		}).done(function (msg) {
			addRow();
			$("#bookingForm").trigger('reset');
			alert("You made a reservation!");
		}).fail(function(data){
				alert("Dublicate reservation!");
			});
	}
	return false;
});
*/

$("#bookingForm").submit(function () {
	var formsData = JSON.stringify($("#bookingForm").serializeObject());
	console.log(formsData);
	if (validateForm()) {
		$.ajax({
			type: "POST",
			url:url+'bookRoom',
			data: formsData,
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			$("#bookingForm").trigger('reset');
			alert("You made a reservation!");

		}).fail(function(data){
				alert("Dublicate reservation!");
			});
	}
	return false;
});
/*
$("#manageReservation").submit(function () {
	var formsData = document.getElementById("reservationId")
	if (validateForm()) {
		$.ajax({
			url: 'http://151.237.128.65:8080/HotelManagerTom/ReservationServlet?id='+formsData,
			type: "GET",
			dataType: 'json',
			contentType: "application/json"
		}).done(function (msg) {
			$("#manageReservation").trigger('reset');
			bookingForms();
			$("bookingForms").loadJSON(msg);
		}).fail(function(data){
			alert("Missing reservation!");
		});
	}
	return false;
});


$("#bookingForms").submit(function () {
	var formsData = JSON.stringify($("#bookingForms").serializeObject());
	console.log(formsData);
	if (validateForm()) {
		$.ajax({
			type: "POST",
			url:'http://151.237.128.65:8080/HotelManagerTom/ReservationServlet',
			data: formsData,
			contentType: "application/json"
		}).done(function (msg) {
			alert("You edit a reservation!");
			$("#bookingForms").trigger('reset');
			
		}).fail(function(data){
			alert("Error!");
		});	}
	return false;
});


*/

$("#manageReservation").submit(function () {
	var formsData = document.getElementById("reservationId")
	console.log(formsData);
	$.ajax({
			url: url+'editReservation',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
			        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
			        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
			    }
		}).done(function (msg) {
			$("#manageReservation").trigger('reset');
			bookingForms();
			console.log(msg);
			$('#bookingForms').loadJSON(msg);
		}).fail(function(data){
			alert("Missing reservation!");
		});
	return false;
});


$("#bookingForms").submit(function () {
	var formsData = JSON.stringify($("#bookingForms").serializeObject());
	console.log(formsData);
	if (validateForm()) {
		$.ajax({
			type: "POST",
			url:url+'editResPost',
			data: formsData,
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			alert("You edit a reservation!");
			$("#bookingForms").trigger('reset');
			bookingFormsBack();
			
		}).fail(function(data){
			alert("Error!");
		});
}
	return false;
});




/*$("#editRoomForm").submit(function () {
	var formsData = document.getElementById("id").value;
	console.log(formsData);

		$.ajax({
			url: 'http://jsonstub.com/elena.bliznakova',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			$("#editRoomForm").trigger('reset');
			addingRooms();
			$('#addingRooms').loadJSON(msg);
		}).fail(function(data){
			alert("Room not exist, try again!");
		});
	
	return false;
});


$("#addingRooms").submit(function () {
	var formData = JSON.stringify($("#addingRooms").serializeObject());
	console.log(formData);
	if (validateForm()) {
		$.ajax({
			type: 'POST',
			url: 'http://151.237.128.65:8080/HotelManagerTom/RoomEditServlet',
			data: formData,
			contentType: 'application/json'
		}).done(function (data) {
			console.log("You edited a room!");
			$("#addingRooms").trigger('reset');
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});
*/
$("#editRoomForm").submit(function () {
	var formsData = document.getElementById("id").value;
	console.log(formsData);

		$.ajax({
			url: url+'editRoom',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			$("#editRoomForm").trigger('reset');
			addingRooms();
			$('#addingRooms').loadJSON(msg);
		}).fail(function(data){
			alert("Room not exist, try again!");
		});
	
	return false;
});


$("#addingRooms").submit(function () {
	var formData = JSON.stringify($("#addingRooms").serializeObject());
	console.log(formData);
	if (validateForm()) {
		$.ajax({
			type: 'POST',
			url: url+'editRoomPost',
			data: formData,
			contentType: 'application/json',
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (data) {
			alert("You edited a room!");
			$("#addingRooms").trigger('reset');
			addingRoomsback();
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});


/*
$("#availabilityForm").submit(function () {
	var formsData = JSON.stringify($("#availabilityForm").serializeObject());
	if (validateForm()) {
		$.ajax({
			url:'http://151.237.128.65:8080/HotelManagerTom/ReservationServlet',
			type: "GET",
			data: formsData,
			dataType: 'json',
			contentType: "application/json"
		}).done(function (msg) {
			console.log(msg);
			for (i= 0; i < msg.length; i++ ){
				var status=msg[i].status;
				console.log(status);
				if(status==1){
					var roomNumber = msg[i].roomId;
					console.log("KOLKO PATI");
					$("<div></div>").attr('class','new').attr('id', roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#c0c0c0";
				
				}else if(status==2){
					
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#80ff80";
					
				}else if(status==3){
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#d0b0cc";
					
				}else if(status==4){
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#bbd9a8";
					
				}
				
			};
			$("#availabilityForm").trigger('reset');
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});


*/


$("#availabilityForm").submit(function () {
	var formsData = JSON.stringify($("#availabilityForm").serializeObject());
	if (validateForm()) {
		$.ajax({
			url:url+'availability',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			console.log(msg);
			for (i= 0; i < msg.length; i++ ){
				var status=msg[i].status;
				console.log(status);
				if(status==1){
					var roomNumber = msg[i].roomId;
					console.log("KOLKO PATI");
					$("<div></div>").attr('class','new').attr('id', roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#c0c0c0";
				
				}else if(status==2){
					
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#80ff80";
					
				}else if(status==3){
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#d0b0cc";
					
				}else if(status==4){
					var roomNumber = msg[i].roomId;
					console.log(roomNumber);
					$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
					document.getElementById(roomNumber).innerHTML = roomNumber;	
					document.getElementById(roomNumber).style.backgroundColor="#bbd9a8";
					
				}
				
			};
			$("#availabilityForm").trigger('reset');
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});


$("#availabilityPeriod").submit(function () {
	var formsData = JSON.stringify($("#availabilityPeriod").serializeObject());
	if (validateForm()) {
		$.ajax({
			url:url+'avPeriod',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			console.log("These are the free rooms!");
			for (i= 0; i < msg.length; i++ ){
				var roomNumber=msg[i].roomId;
				var beds=msg[i].beds;
				$("<div></div>").attr('class','new').attr('id',roomNumber).appendTo('Body'); 
				document.getElementById(roomNumber).innerHTML = roomNumber+"</br>"+beds+"beds";
			}
			$("#availabilityPeriod").trigger('reset');
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});



$("#reservationList").submit(function () {
	var formsData = JSON.stringify($("#reservationList").serializeObject());
	if (validateForm()) {
		$.ajax({
			url:url+'reservationId',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			console.log(msg);
			console.log("These are the reservations rooms!");
			for (i= 0; i < msg.length; i++ ){
				var roomNumber=msg[i].roomId;
				var reservationId=msg[i].reservationId;	
				$("<div></div>").attr('class','news').attr('id',roomNumber).appendTo('Body'); 
				document.getElementById(roomNumber).innerHTML = roomNumber+"</br>"+"ID:"+reservationId;
			}
			$("#reservationList").trigger('reset');
		}).fail(function(data){
			alert("Error!");
		});
	}
	return false;
});

$("#viewRoomForm").submit(function () {
		$.ajax({
			url: url+'editRoom',
			type: "GET",
			dataType: 'json',
			contentType: "application/json",
			beforeSend: function (request) {
		        request.setRequestHeader('JsonStub-User-Key', '3cd40d30-3de8-4da4-a3cc-143de8510ee3');
		        request.setRequestHeader('JsonStub-Project-Key', 'bd48037a-5db2-4e6d-be3e-7293baf1a712');
		    }
		}).done(function (msg) {
			console.log(msg);
			viewRooms();
			$('#viewRooms').loadJSON(msg);
			$("#viewRoomForm").trigger('reset');
		}).fail(function(data){
			alert("Room not exist, try again!");
		});
	
	return false;
});


$("#viewRooms").submit(function () {
	viewRoomsBack();
return false;
});
